### 菜单功能说明

### NC 客户管理

1. 从 NC 系统中导入的客户

### 开票客户管理

1. 在开票时, 为了便于快速录入 购买方的信息. 需要事前录入客户
2. 开票时, 在购买方的 名称栏, 选择 开票客户后，自动填入 税号 地址 开户行等信息。
3. 开票客户 可以从 基于已经导入的 NC 客户 创建。以减少维护的工作量

### 客户标签管理

1. 对开票客户的 分类管理。 一个客户可以打多个 标签. 便于查找

### NC 商品管理

1. 从 NC 系统中导入的商品

### 开票商品管理

1. 开票时，为了便于录入产品。事前维护的商品信息
2. 开票时，添加明细行，在商品名称栏，选择商品后，自动填入 规格/单位/单价/税率
3. 开票商品 可以基于 已经导入的 NC 商品创建，以减少维护的工作量

### 开票商品类别

1. 税局确定的商品分类. 不同的商品 税率不同
2. 开票商品 应关联 开票商品类别

### 进项发票

1. 通过 手工录入 或者 OCR 扫描识别的进项票
2. 扫描结果暂存。 扫描识别 可能存在数据错误. 数据应人工确认 或者 查验确认
3. 进项项录入后, 后续可以进行查验真伪
4. 经查验后, 税局返回 发票数据. 该数据为准确的数据。应据此修改扫描的结果
5. 合格正确的发票数据, 返回 NC 系统中.
   NC 系统中据此 自动转为 财务会计凭证。并且和 采购订单关联

### 进项红票

1. 我们给供应商退货时. 供应商应该给我们提供 进项红票
2. 进项红票的处理 和 进项蓝票 相似，应该也有扫描和查验 (这点需要确认)
3. 进项红票 应该与进项蓝票 关联
4. 进项红票数据 返回到 NC 系统中

### 销项发票

1. NC 系统中,

### 销项红票
