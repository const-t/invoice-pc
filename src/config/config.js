import { addons_list as addons_list2 } from './menus'
import { components_for_router } from './menus'
import { menus_tree_get as menus_tree_get2 } from './menus'
import { menus_list_get as menus_list_get2 } from './menus'

export const baseURL = process.env.VUE_APP_BASE_API
export const timeout = 50000

export const addons_list = addons_list2
export const components = components_for_router

export const menus_tree_get = () => {
  const odoo_menus = menus_tree_get2()
  return odoo_menus
}

export const menus_list_get = () => {
  const odoo_menus_list = menus_list_get2()
  return odoo_menus_list
}

export const app_title = 'XX发票管理平台'
export const app_footer = 'XX发票管理平台 ©2022 北京百旺金科信息技术有限公司'
