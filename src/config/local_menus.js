export const local_components_for_router = {
  'fp_setting.action_user': () => import('@/fapiao_ui/fp_setting/action_user'),

  'fapiao.action_in_fapiao': () =>
    import('@/fapiao_ui/fapiao/action_in_fapiao'),
  'fapiao.action_in_refund': () =>
    import('@/fapiao_ui/fapiao/action_in_refund'),
  'fapiao.action_out_fapiao': () =>
    import('@/fapiao_ui/fapiao/action_out_fapiao'),
  'fapiao.action_out_refund': () =>
    import('@/fapiao_ui/fapiao/action_out_refund'),

  'fp_sale.action_sale_order': () =>
    import('@/fapiao_ui/fp_sale/action_sale_order')
}

export const local_menus_tree = [
  // {
  //   id: 'test',
  //   icon: 'shopping',
  //   theme: 'twoTone',
  //   name: '测试用菜单',
  //   children: [
  //     {
  //       action: 'base.action_country',
  //       id: 'base.action_country',
  //       icon: 'shopping',
  //       theme: 'twoTone',
  //       name: '国家'
  //     },

  //     {
  //       action: 'base.action_country_group',
  //       id: 'base.action_country_group',
  //       icon: 'shopping',
  //       theme: 'twoTone',
  //       name: '国家组'
  //     }
  //   ]
  // },
  {
    id: 'fp_in_fapiao',
    icon: 'shopping',
    theme: 'twoTone',
    name: '进项发票管理',
    children: [
      {
        action: 'fapiao.action_in_fapiao',
        id: 'fp_in_fapiao.menu_action_in_fapiao',
        icon: 'shopping',
        theme: 'twoTone',
        name: '进项发票'
      },
      {
        action: 'fapiao.action_in_refund',
        id: 'fp_in_fapiao.menu_action_in_refund',
        icon: 'shopping',
        theme: 'twoTone',
        name: '进项红票'
      }
    ]
  },
  // {
  //   id: 'fp_nuonuo_out_fapiao',
  //   icon: 'shopping',
  //   theme: 'twoTone',
  //   name: '已开发票管理',

  //   children: [
  //     // {
  //     //   action: 'fp_nuonuo_out_fapiao.action_out_fapiao',
  //     //   id: 'fp_nuonuo_out_fapiao.menu_action_out_fapiao',
  //     //   icon: 'shopping',
  //     //   theme: 'twoTone',
  //     //   name: '销项发票'
  //     // },
  //     // {
  //     //   action: 'fapiao.action_out_refund',
  //     //   id: 'fp_nuonuo_out_fapiao.menu_action_out_refund',
  //     //   icon: 'shopping',
  //     //   theme: 'twoTone',
  //     //   name: '销项红票'
  //     // }
  //   ]
  // },
  {
    id: 'fp_out_fapiao',
    icon: 'shopping',
    theme: 'twoTone',
    name: '销项发票管理',

    children: [
      {
        action: 'fapiao.action_out_fapiao',
        id: 'fp_out_fapiao.menu_action_out_fapiao',
        icon: 'shopping',
        theme: 'twoTone',
        name: '销项发票'
      },
      {
        action: 'fapiao.action_out_refund',
        id: 'fp_out_fapiao.menu_action_out_refund',
        icon: 'shopping',
        theme: 'twoTone',
        name: '销项红票'
      }
    ]
  },

  {
    id: 'fp_sale',
    icon: 'shopping',
    theme: 'twoTone',
    name: '销售管理',

    children: [
      {
        action: 'fp_sale.action_sale_order',
        id: 'fp_sale.menu_action_sale',
        icon: 'shopping',
        theme: 'twoTone',
        name: '销售订单'
      }
    ]
  },

  {
    id: 'fp_product',
    icon: 'shopping',
    theme: 'twoTone',
    name: '商品管理',
    children: [
      {
        action: 'fapiao.action_product',
        id: 'fp_product.menu_product',
        icon: 'shopping',
        theme: 'twoTone',
        name: '商品管理'
      },

      {
        action: 'fapiao.action_product_category',
        id: 'fp_product.menu_product_category',
        icon: 'shopping',
        theme: 'twoTone',
        name: '开票商品类别'
      }
    ]
  },

  {
    id: 'fp_partner',
    icon: 'shopping',
    theme: 'twoTone',
    name: '客户管理',
    children: [
      {
        action: 'fapiao.action_partner',
        id: 'fp_partner.menu_partner',
        icon: 'shopping',
        theme: 'twoTone',
        name: '开票客户管理'
      },
      {
        action: 'fapiao.action_partner_category',
        id: 'fp_partner.menu_action_partner_category',
        icon: 'shopping',
        theme: 'twoTone',
        name: '客户标签管理'
      }
    ]
  },

  {
    id: 'fp_setting',
    icon: 'shopping',
    theme: 'twoTone',
    name: '系统设置',
    children: [
      {
        action: 'fp_setting.action_company',
        id: 'fp_setting.menu_action_company',
        icon: 'shopping',
        theme: 'twoTone',
        name: '公司'
      },
      {
        action: 'fp_setting.action_user',
        id: 'fp_setting.menu_action_user',
        icon: 'shopping',
        theme: 'twoTone',
        name: '用户'
      }
    ]
  }
]
