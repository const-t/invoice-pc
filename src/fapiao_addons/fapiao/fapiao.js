const readonly_for_fapiao_partner = ({ record }) => {
  const { move_type } = record
  if (['out_invoice', 'out_refund'].includes(move_type)) {
    return 1
  } else {
    return undefined
  }
}

export default {
  tree_fp_fapiao: {
    _odoo_model: 'ir.ui.view',
    model: 'fp.fapiao',
    type: 'tree',

    fields: {
      name: { invisible: 1 },
      move_type: { string: '进销项类型', invisible: 0 },
      invoice_type: { string: '种类' },
      state: { string: '状态' },
      company_id: { string: '公司' },

      buyer_name: { string: '购买方' },
      // buyer_tax_code: { string: '税号' },
      // buyer_address_phone: { string: '地址电话' },
      // buyer_bank_account: { string: '银行账户' },

      saler_name: { string: '销售方' },
      // saler_tax_code: { string: '税号' },
      // saler_address_phone: { string: '地址电话' },
      // saler_bank_account: { string: '银行账户' },

      // name: { string: '名称' },
      // payee_name: { string: '收款人' },
      // checker_name: { string: '复核' },
      // drawer_name: { string: '开票人' },

      type_code: { string: '发票代码' },
      code: { string: '发票号码' },
      // check_code: { string: '校验码' },
      date: { string: '开票日期' },
      // note: { string: '备注' },
      // amount_untaxed: { string: '不含税合计' },
      // amount_tax: { string: '税合计' },
      amount_total: { string: '价税合计' }
      // cn_amount_total: { string: '价税合计大写' }
    }
  },

  form_fp_fapiao: {
    _odoo_model: 'ir.ui.view',
    model: 'fp.fapiao',
    type: 'form',

    arch: {
      header: {
        buttons: [
          {
            name: 'action_confirm',
            string: '确认',
            type: 'object',
            btn_type: 'primary',
            invisible: ({ record }) => {
              const { state } = record
              return state !== 'draft'
            }
          },
          // {
          //   name: 'action_check',
          //   string: '查验',
          //   type: 'object',
          //   invisible: ({ record }) => {
          //     const { state, move_type } = record
          //     return (
          //       state !== 'confirmed' ||
          //       !['in_invoice', 'in_refund'].includes(move_type)
          //     )
          //   }
          // },
          {
            name: 'action_set_checked',
            string: '人工查验',
            type: 'object',
            invisible: ({ record }) => {
              const { state, move_type } = record
              return (
                state !== 'confirmed' ||
                !['in_invoice', 'in_refund'].includes(move_type)
              )
            }
          },
          // {
          //   name: 'action_print',
          //   string: '开票',
          //   type: 'object',
          //   invisible: ({ record }) => {
          //     const { state, move_type } = record
          //     return (
          //       state !== 'confirmed' ||
          //       !['out_invoice', 'out_refund'].includes(move_type)
          //     )
          //   }
          // },
          {
            name: 'action_post',
            string: '过账',
            type: 'object',
            btn_type: 'primary',
            invisible: ({ record }) => {
              const { state } = record
              return !['checked', 'printed', 'confirmed'].includes(state)
            }
          },

          {
            name: 'action_cancel',
            string: '取消',
            type: 'object',
            invisible: ({ record }) => {
              const { state } = record
              return state === 'cancel'
            }
          },
          {
            name: 'action_draft',
            string: '重置为草稿',
            type: 'object',
            invisible: ({ record }) => {
              const { state } = record
              return state !== 'cancel'
            }
          }
        ],
        fields: {
          state: {
            widget: 'statusbar',
            statusbar_visible({ record }) {
              const { move_type } = record
              if (['out_invoice', 'out_refund'].includes(move_type)) {
                return 'draft,confirmed,printing,printed,posted'
              } else {
                return 'draft,confirmed,checking,checked,posted'
              }
            }
          }
        }
      }
    },

    fields: {
      fapiao_serial_num: { string: '发票序列号' },
      api_status: { string: 'api状态' },
      api_status_msg: { string: 'api状态消息' },
      api_fail_cause: { string: 'api失败原因' },
      date_fapiao: { string: '开票日期' },

      move_type: { string: '进销项类型' },
      invoice_type: { string: '种类' },
      state: {
        string: '状态',
        selection: [
          ['draft', '草稿'],
          ['confirmed', '已确认'],
          ['checking', '查验中'],
          ['checked', '已查验'],
          ['checkerror', '查验失败'],
          ['printing', '开票中'],
          ['printed', '已开票'],
          ['printerror', '开票失败'],
          ['posted', '已过账'],
          ['cancel', '已取消']
        ]
      },

      name: { string: '订单号码', readonly: 0 },

      company_id: { string: '公司' },

      attachment_id: { string: '原始单据', widget: 'attachment' },

      partner_id: {
        string: '业务伙伴',
        required: readonly_for_fapiao_partner,
        invisible({ record }) {
          return !readonly_for_fapiao_partner({ record })
        }
      },

      buyer_name: { string: '购买方', readonly: readonly_for_fapiao_partner },
      buyer_tax_code: { string: '税号', readonly: readonly_for_fapiao_partner },
      buyer_phone: {
        string: '电话',
        readonly: readonly_for_fapiao_partner
      },
      buyer_address: {
        string: '地址',
        readonly: readonly_for_fapiao_partner
      },

      buyer_address_phone: {
        string: '地址电话',
        readonly: readonly_for_fapiao_partner
      },
      buyer_bank_account: {
        string: '银行账户',
        readonly: readonly_for_fapiao_partner
      },

      saler_name: { string: '销售方', readonly: readonly_for_fapiao_partner },
      saler_tax_code: {
        string: '税号',
        readonly: readonly_for_fapiao_partner
      },
      saler_phone: {
        string: '电话',
        readonly: readonly_for_fapiao_partner
      },
      saler_address: {
        string: '地址',
        readonly: readonly_for_fapiao_partner
      },
      saler_address_phone: {
        string: '地址电话',
        readonly: readonly_for_fapiao_partner
      },
      saler_bank_account: {
        string: '银行账户',
        readonly: readonly_for_fapiao_partner
      },

      payee_name: { string: '收款人' },
      checker_name: { string: '复核' },
      drawer_name: { string: '开票人' },

      type_code: { string: '发票代码' },
      code: { string: '发票号码' },
      check_code: { string: '校验码' },
      date: { string: '日期' },
      date_order: { string: '订单日期' },
      note: { string: '备注' },
      amount_untaxed: { string: '不含税合计' },
      amount_tax: { string: '税合计' },
      amount_total: { string: '价税合计' },
      cn_amount_total: { string: '价税合计大写' },

      line_ids: {
        widget: 'x2many_tree',
        context: {},
        string: '明细行',

        views: {
          tree: {
            fields: {
              // sequence: { string: '序号' },
              // name: { string: '说明' },
              // fp_product_id: { string: '产品' },
              product_categ_name: { string: '商品类别' },
              product_name: { string: '货物或应税劳务、服务名称' },
              product_spec: { string: '规格型号' },
              uom_name: { string: '单位' },

              quantity: { string: '数量' },
              price_unit: { string: '单价' },

              tax_ratio: { string: '税率' },
              amount_untaxed: { string: '金额' },
              amount_tax: { string: '税额' }
              // amount_total: { string: '含税金额' }
            }
          },

          form: {
            fields: {
              move_type: { invisible: 1 },
              sequence: { string: '序号' },
              name: { string: '说明' },
              product_id: {
                string: '产品',
                required: readonly_for_fapiao_partner,
                invisible({ record }) {
                  return !readonly_for_fapiao_partner({ record })
                }
              },
              product_categ_code: {
                string: '商品类别编码',
                readonly: readonly_for_fapiao_partner
              },

              product_categ_name: {
                string: '商品类别名称',
                readonly: readonly_for_fapiao_partner
              },

              product_name: {
                string: '货物或应税劳务、服务名称',
                readonly: readonly_for_fapiao_partner
              },
              product_spec: {
                string: '规格型号',
                readonly: readonly_for_fapiao_partner
              },
              uom_name: {
                string: '单位',
                readonly: readonly_for_fapiao_partner
              },

              quantity: { string: '数量' },
              price_unit: { string: '单价' },

              tax_ratio: { string: '税率' },
              amount_untaxed: { string: '金额' },
              amount_tax: { string: '税额' }
              // amount_total: { string: '含税金额' }
            }
          }
        }
      }
    }
  }
}
