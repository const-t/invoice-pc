export default {
  search_in_refund: {
    _odoo_model: 'ir.ui.view',
    model: 'account.account',
    type: 'fp.fapiao',
    arch: {
      fields: {
        name: {
          filter_domain: self => {
            return [
              '|',
              ['saler_name', 'ilike', self],
              ['buyer_name', '=like', self],
              ['code', '=like', self]
            ]
          }
        }
      },

      filters: {
        group1: {
          draft: {
            string: '草稿',
            domain: [['state', '=', 'draft']]
          },
          posted: {
            string: '已查验',
            domain: [['state', '=', 'posted']]
          }
        },

        group2: {
          date: { string: '日期', date: 'date' }
        }
      }
    }
  },

  action_in_refund: {
    _odoo_model: 'ir.actions',
    name: '进项红票',
    type: 'ir.actions.act_window',
    res_model: 'fp.fapiao',
    search_view_id: 'search_in_refund',
    domain: [['move_type', '=', 'in_refund']],
    context: { default_move_type: 'in_refund' },
    views: {
      tree: 'fapiao.tree_fp_fapiao',
      form: 'fapiao.form_fp_fapiao'
    }
  }
}
