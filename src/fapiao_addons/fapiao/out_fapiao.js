export default {
  search_out_fapiao: {
    _odoo_model: 'ir.ui.view',
    model: 'account.account',
    type: 'fp.fapiao',
    arch: {
      fields: {
        name: {
          filter_domain: self => {
            return [
              '|',
              ['saler_name', 'ilike', self],
              ['buyer_name', '=like', self],
              ['code', '=like', self]
            ]
          }
        }
      },

      filters: {
        group1: {
          draft: {
            string: '草稿',
            domain: [['state', '=', 'draft']]
          },
          posted: {
            string: '已查验',
            domain: [['state', '=', 'posted']]
          }
        },

        group2: {
          date: { string: '日期', date: 'date' }
        }
      }
    }
  },

  action_out_fapiao: {
    _odoo_model: 'ir.actions',
    name: '销项发票',
    type: 'ir.actions.act_window',
    res_model: 'fp.fapiao',
    search_view_id: 'search_out_fapiao',
    domain: [['move_type', '=', 'out_invoice']],
    context: { default_move_type: 'out_invoice' },
    views: {
      tree: 'fapiao.tree_fp_fapiao',
      form: 'fapiao.form_fp_fapiao'
    }
  }
}
