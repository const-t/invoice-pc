export default {
  tree_partner: {
    _odoo_model: 'ir.ui.view',
    model: 'res.partner',
    type: 'tree',

    fields: {
      name: {},
      email: {},
      ref: {},
      company_id: {}
      // street2: {}
      // bank_ids: {}
    }
  },

  form_partner: {
    _odoo_model: 'ir.ui.view',
    model: 'res.partner',
    type: 'form',

    fields: {
      name: {},
      email: {},
      ref: {},
      street2: { string: '地址' },
      phone: {},
      vat: {},
      bank_ids: {
        widget: 'x2many_tree',
        views: {
          tree: {
            fields: {
              sequence: {},
              bank_id: {},
              acc_number: {}
            }
          },
          form: {
            fields: {
              sequence: {},
              bank_id: {},
              acc_number: {}
            }
          }
        }
      },
      company_id: {}
    }
  },

  action_partner: {
    _odoo_model: 'ir.actions',
    name: '客户',
    type: 'ir.actions.act_window',
    res_model: 'res.partner',
    domain: [['customer_rank', '>', 0]],
    context: { default_customer_rank: 1, default_is_company: true },
    views: {
      tree: 'tree_partner',
      form: 'form_partner'
    }
  }
}
