export default {
  tree_partner_category: {
    _odoo_model: 'ir.ui.view',
    model: 'res.partner.category',
    type: 'tree',

    fields: {
      display_name: { string: '全名' },
      name: { string: '名称' },
      parent_id: { string: '上级分类' }
    }
  },

  form_partner_category: {
    _odoo_model: 'ir.ui.view',
    model: 'res.partner.category',
    type: 'form',

    fields: {
      // display_name: {},
      name: {},
      // color: {},
      parent_id: {},
      active: {}
    }
  },

  action_partner_category: {
    _odoo_model: 'ir.actions',
    name: '客户标签',
    type: 'ir.actions.act_window',
    res_model: 'res.partner.category',
    domain: [],
    context: {},
    views: {
      tree: 'tree_partner_category',
      form: 'form_partner_category'
    }
  }
}
