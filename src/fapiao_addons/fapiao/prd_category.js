export default {
  tree_prd_category: {
    _odoo_model: 'ir.ui.view',
    model: 'product.category',
    type: 'tree',

    fields: {
      display_name: { string: '全名' },
      name: { string: '名称' },
      code: { string: '编码' },
      parent_id: { string: '上级分类' }
      // group
    }
  },

  form_prd_category: {
    _odoo_model: 'ir.ui.view',
    model: 'product.category',
    type: 'form',

    fields: {
      display_name: { string: '全名' },
      name: { string: '名称' },
      code: { string: '编码' },
      parent_id: { string: '上级分类' }
    }
  },

  action_product_category: {
    _odoo_model: 'ir.actions',
    name: '商品分类',
    type: 'ir.actions.act_window',
    res_model: 'product.category',
    domain: [['group', '=', 'fapiao']],
    context: { default_group: 'fapiao' },
    views: {
      tree: 'tree_prd_category',
      form: 'form_prd_category'
    }
  }
}
