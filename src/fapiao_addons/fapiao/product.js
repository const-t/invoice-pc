export default {
  tree_product: {
    _odoo_model: 'ir.ui.view',
    model: 'product.template',
    type: 'tree',

    fields: {
      name: {},
      default_code: {},
      company_id: {}
      // barcode: {},
      // list_price: {},
      // standard_price: {},
      // categ_id: {},
      // detailed_type: {},
      // uom_id: {},
      // spec: {},
      // tax_ratio: {}
    }
  },

  form_product: {
    _odoo_model: 'ir.ui.view',
    model: 'product.template',
    type: 'form',

    fields: {
      name: {},
      default_code: {},
      barcode: {},
      company_id: {},
      list_price: {},
      standard_price: {},
      categ_id: {
        required: 1,
        domain: [['group', '=', 'fapiao']]
      },
      categ_code: {},
      detailed_type: {},
      uom_id: {},
      spec: {},
      tax_ratio: {}
    }
  },

  action_product: {
    _odoo_model: 'ir.actions',
    name: '商品',
    type: 'ir.actions.act_window',
    res_model: 'product.template',
    domain: [['categ_id.group', '=', 'fapiao']],
    context: { categ_id_group: 'fapiao' },
    views: {
      tree: 'tree_product',
      form: 'form_product'
    }
  }
}
