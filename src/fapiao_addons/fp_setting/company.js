// ok

export default {
  view_company_tree: {
    _odoo_model: 'ir.ui.view',
    model: 'res.company',
    type: 'tree',
    buttons: { create: false, edit: false, delete: false },
    fields: {
      name: {},
      parent_id: {},
      partner_id: {},
      company_registry: {}
    }
  },

  view_company_form: {
    _odoo_model: 'ir.ui.view',
    model: 'res.company',
    type: 'form',
    buttons: { create: false, edit: true, delete: false },

    fields: {
      name: {},
      parent_id: {},
      partner_id: {},
      logo: { widget: 'image' },
      company_registry: {},
      street2: {},
      phone: {},
      vat: {},
      nuonuo_appkey: {},
      nuonuo_app_secret: {},
      nuonuo_token: {},
      nuonuo_taxnum: { readonly: 0 },
      bank_ids: {
        widget: 'x2many_tree',
        readonly: 1,
        views: {
          tree: {
            fields: {
              sequence: {},
              bank_id: {},
              acc_number: {}
            }
          },
          form: {
            fields: {
              sequence: {},
              bank_id: {},
              acc_number: {}
            }
          }
        }
      },

      child_ids: { widget: 'x2many_tree' }
    }
  },

  action_company: {
    _odoo_model: 'ir.actions',
    name: '公司',
    type: 'ir.actions.act_window',
    res_model: 'res.company',
    domain: [],
    context: {},
    views: {
      tree: 'view_company_tree',
      form: 'view_company_form'
    }
  }
}
