// ok

export default {
  view_user_tree: {
    _odoo_model: 'ir.ui.view',
    model: 'res.users',
    type: 'tree',
    buttons: { create: false, edit: false, delete: false },
    fields: {
      login: {},
      name: {},
      email: {},
      ref: {},
      mobile: {},
      company_id: {},
      active: {},
      login_date: {},
      company_ids: {
        widget: 'many2many_tags'
      }
    }
  },

  view_user_form: {
    _odoo_model: 'ir.ui.view',
    model: 'res.users',
    type: 'form',
    buttons: { create: false, edit: true, delete: false },
    fields: {
      login: {},
      name: {},
      email: {},
      ref: {},
      mobile: {},
      company_id: {},
      active: {},
      login_date: {},
      company_ids: {
        widget: 'many2many_tags'
      }
    }
  },

  action_user: {
    _odoo_model: 'ir.actions',
    name: '公司',
    type: 'ir.actions.act_window',
    res_model: 'res.users',
    domain: [],
    context: {},
    views: {
      tree: 'view_user_tree',
      form: 'view_user_form'
    }
  }
}
